class PeriodDTO {
    private String start
    private String end

    String getStart() {
        return start
    }

    void setStart(String start) {
        this.start = start
    }

    String getEnd() {
        return end
    }

    void setEnd(String end) {
        this.end = end
    }
}

class AddPatientDTO {
    private String resourceType
    private List<IdentifierDTO> identifier
    private List<NameDTO> name
    private List<TelecomDTO> telecom
    private String gender
    private String birthDate
    private boolean deceasedBoolean
    private List<AddressDTO> address
    private List<MaritalStatusDTO> maritalStatus
    private List<GeneralPractitionerDTO> generalPractitioner
    private List<ExtensionDTO> extension


    String getResourceType() {
        return resourceType
    }

    void setResourceType(String resourceType) {
        this.resourceType = resourceType
    }

    List<IdentifierDTO> getIdentifier() {
        return identifier
    }

    void setIdentifier(List<IdentifierDTO> identifier) {
        this.identifier = identifier
    }

    List<NameDTO> getName() {
        return name
    }

    void setName(List<NameDTO> name) {
        this.name = name
    }

    List<TelecomDTO> getTelecom() {
        return telecom
    }

    void setTelecom(List<TelecomDTO> telecom) {
        this.telecom = telecom
    }

    String getGender() {
        return gender
    }

    void setGender(String gender) {
        this.gender = gender
    }

    String getBirthDate() {
        return birthDate
    }

    void setBirthDate(String birthDate) {
        this.birthDate = birthDate
    }

    boolean getDeceasedBoolean() {
        return deceasedBoolean
    }

    void setDeceasedBoolean(boolean deceasedBoolean) {
        this.deceasedBoolean = deceasedBoolean
    }

    List<AddressDTO> getAddress() {
        return address
    }

    void setAddress(List<AddressDTO> address) {
        this.address = address
    }

    List<MaritalStatusDTO> getMaritialStatus() {
        return maritalStatus
    }

    void setMaritialStatus(List<MaritalStatusDTO> maritialStatus) {
        this.maritalStatus = maritialStatus
    }

    List<GeneralPractitionerDTO> getGeneralPractitioner() {
        return generalPractitioner
    }

    void setGeneralPractitioner(List<GeneralPractitionerDTO> generalPractitioner) {
        this.generalPractitioner = generalPractitioner
    }

    List<ExtensionDTO> getExtension() {
        return extension
    }

    void setExtension(List<ExtensionDTO> extension) {
        this.extension = extension
    }

    @Override
    public String toString() {
        return "AddPatientDTO{" +
                "resourceType='" + resourceType + '\'' +
                ", identifier=" + identifier +
                ", name=" + name +
                ", telecom=" + telecom +
                ", gender='" + gender + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", deceasedBoolean=" + deceasedBoolean +
                ", address=" + address +
                ", maritialStatus=" + maritalStatus +
                ", generalPractitioner=" + generalPractitioner +
                ", extension=" + extension +
                '}';
    }
}

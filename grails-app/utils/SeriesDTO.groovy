class SeriesDTO {
    private String uid
    private List<ModalityListDTO> modality
    private String description
    private String started

    String getUid() {
        return uid
    }

    void setUid(String uid) {
        this.uid = uid
    }

    List<ModalityListDTO> getModality() {
        return modality
    }

    void setModality(List<ModalityListDTO> modality) {
        this.modality = modality
    }

    String getDescription() {
        return description
    }

    void setDescription(String description) {
        this.description = description
    }

    String getStarted() {
        return started
    }

    void setStarted(String started) {
        this.started = started
    }
}

class ExtensionDTO {
    private String valueDate
    private int valueInteger

    String getValueDate() {
        return valueDate
    }

    void setValueDate(String valueDate) {
        this.valueDate = valueDate
    }

    int getValueInteger() {
        return valueInteger
    }

    void setValueInteger(int valueInteger) {
        this.valueInteger = valueInteger
    }
}

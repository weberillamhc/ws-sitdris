class SimpleDTO {
    private String text;
    private String code;

    String getText() {
        return text
    }

    void setText(String text) {
        this.text = text
    }

    String getCode() {
        return code
    }

    void setCode(String code) {
        this.code = code
    }
}

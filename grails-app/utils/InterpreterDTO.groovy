class InterpreterDTO {
    private String resourceType
    private List<NameDTO> name

    String getResourceType() {
        return resourceType
    }

    void setResourceType(String resourceType) {
        this.resourceType = resourceType
    }

    List<NameDTO> getName() {
        return name
    }

    void setName(List<NameDTO> name) {
        this.name = name
    }
}

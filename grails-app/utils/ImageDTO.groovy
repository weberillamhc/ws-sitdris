class ImageDTO {
    private String resourceType
    private String uid
    private List<IdentifierDTO> identifier
    private List<ModalityListDTO> modalityList
    private List<PatientDTO> patient
    private String started
    private List<EndPointDTO> endpoint
    private List<SeriesDTO> series

    String getResourceType() {
        return resourceType
    }

    void setResourceType(String resourceType) {
        this.resourceType = resourceType
    }

    String getUid() {
        return uid
    }

    void setUid(String uid) {
        this.uid = uid
    }

    List<IdentifierDTO> getIdentifier() {
        return identifier
    }

    void setIdentifier(List<IdentifierDTO> identifier) {
        this.identifier = identifier
    }

    List<ModalityListDTO> getModalityList() {
        return modalityList
    }

    void setModalityList(List<ModalityListDTO> modalityList) {
        this.modalityList = modalityList
    }

    List<PatientDTO> getPatient() {
        return patient
    }

    void setPatient(List<PatientDTO> patient) {
        this.patient = patient
    }

    String getStarted() {
        return started
    }

    void setStarted(String started) {
        this.started = started
    }

    List<EndPointDTO> getEndpoint() {
        return endpoint
    }

    void setEndpoint(List<EndPointDTO> endpoint) {
        this.endpoint = endpoint
    }

    List<SeriesDTO> getSeries() {
        return series
    }

    void setSeries(List<SeriesDTO> series) {
        this.series = series
    }
}

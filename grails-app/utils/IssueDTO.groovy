class IssueDTO {
    private String severity;
    private SimpleDTO details;

    String getSeverity() {
        return severity
    }

    void setSeverity(String severity) {
        this.severity = severity
    }

    SimpleDTO getDetails() {
        return details
    }

    void setDetails(SimpleDTO details) {
        this.details = details
    }
}

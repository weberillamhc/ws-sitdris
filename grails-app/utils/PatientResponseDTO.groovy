class PatientResponseDTO {
    private String resourceType;
    private String id;
    private String type;
    private List<EntryResponseDTO> entry;

    String getResourceType() {
        return resourceType
    }

    void setResourceType(String resourceType) {
        this.resourceType = resourceType
    }

    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }

    String getType() {
        return type
    }

    void setType(String type) {
        this.type = type
    }

    List<EntryResponseDTO> getEntry() {
        return entry
    }

    void setEntry(List<EntryResponseDTO> entry) {
        this.entry = entry
    }
}

class GeneralPractitionerDTO {
    private String resourceType
    private List<IdentifierDTO> identifier

    String getResourceType() {
        return resourceType
    }

    void setResourceType(String resourceType) {
        this.resourceType = resourceType
    }

    List<IdentifierDTO> getIdentifier() {
        return identifier
    }

    void setIdentifier(List<IdentifierDTO> identifier) {
        this.identifier = identifier
    }
}

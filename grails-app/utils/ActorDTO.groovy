class ActorDTO {

    private String reference;
    private String id;

    String getReference() {
        return reference
    }

    void setReference(String reference) {
        this.reference = reference
    }

    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }
}

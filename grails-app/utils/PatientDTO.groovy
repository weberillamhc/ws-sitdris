class PatientDTO {
    private String resourceType
    private String id

    String getResourceType() {
        return resourceType
    }

    void setResourceType(String resourceType) {
        this.resourceType = resourceType
    }

    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }
}

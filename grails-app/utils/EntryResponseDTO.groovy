class EntryResponseDTO {
    private ResourceDTO resource;
    private ResponseDTO response;

    ResourceDTO getResource() {
        return resource
    }

    void setResource(ResourceDTO resource) {
        this.resource = resource
    }

    ResponseDTO getResponse() {
        return response
    }

    void setResponse(ResponseDTO response) {
        this.response = response
    }
}

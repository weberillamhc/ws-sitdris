class ResponseDTO {
    private String status;
    private OutComeDTO outcome;

    String getStatus() {
        return status
    }

    void setStatus(String status) {
        this.status = status
    }

    OutComeDTO getOutcome() {
        return outcome
    }

    void setOutcome(OutComeDTO outcome) {
        this.outcome = outcome
    }
}

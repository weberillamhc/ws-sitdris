class OutComeDTO {
    private String resourceType;
    private List<IssueDTO> issue;

    String getResourceType() {
        return resourceType
    }

    void setResourceType(String resourceType) {
        this.resourceType = resourceType
    }

    List<IssueDTO> getIssue() {
        return issue
    }

    void setIssue(List<IssueDTO> issue) {
        this.issue = issue
    }
}

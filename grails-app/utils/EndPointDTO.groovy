class EndPointDTO {
    private String status
    private List<String> connectionType
    private List<String> payloadType
    private String address

    String getStatus() {
        return status
    }

    void setStatus(String status) {
        this.status = status
    }

    List<String> getConnectionType() {
        return connectionType
    }

    void setConnectionType(List<String> connectionType) {
        this.connectionType = connectionType
    }

    List<String> getPayloadType() {
        return payloadType
    }

    void setPayloadType(List<String> payloadType) {
        this.payloadType = payloadType
    }

    String getAddress() {
        return address
    }

    void setAddress(String address) {
        this.address = address
    }
}

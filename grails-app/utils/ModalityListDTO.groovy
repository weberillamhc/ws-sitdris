class ModalityListDTO {
    private String code

    String getCode() {
        return code
    }

    void setCode(String code) {
        this.code = code
    }
}

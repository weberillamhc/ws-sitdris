class TelecomDTO {
    private String resourceType
    private String system
    private String value

    String getResourceType() {
        return resourceType
    }

    void setResourceType(String resourceType) {
        this.resourceType = resourceType
    }

    String getSystem() {
        return system
    }

    void setSystem(String system) {
        this.system = system
    }

    String getValue() {
        return value
    }

    void setValue(String value) {
        this.value = value
    }
}

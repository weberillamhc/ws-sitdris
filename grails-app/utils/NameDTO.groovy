class NameDTO {
    private String resourceType
    private String family
    private List<String> given
    private String suffix

    String getResourceType() {
        return resourceType
    }

    void setResourceType(String resourceType) {
        this.resourceType = resourceType
    }

    String getFamily() {
        return family
    }

    void setFamily(String family) {
        this.family = family
    }

    List<String> getGiven() {
        return given
    }

    void setGiven(List<String> given) {
        this.given = given
    }

    String getSuffix() {
        return suffix
    }

    void setSuffix(String suffix) {
        this.suffix = suffix
    }
}

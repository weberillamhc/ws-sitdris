class ParticipantDTO {
    private List<SimpleDTO> type;
    private ActorDTO actor;
    private String required;
    private String status;

    List<SimpleDTO> getType() {
        return type
    }

    void setType(List<SimpleDTO> type) {
        this.type = type
    }

    ActorDTO getActor() {
        return actor
    }

    void setActor(ActorDTO actor) {
        this.actor = actor
    }

    String getRequired() {
        return required
    }

    void setRequired(String required) {
        this.required = required
    }

    String getStatus() {
        return status
    }

    void setStatus(String status) {
        this.status = status
    }
}

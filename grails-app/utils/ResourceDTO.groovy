class ResourceDTO {
    private String resourceType;
    private String comment;
    private ActorDTO appointment;

    String getResourceType() {
        return resourceType
    }

    void setResourceType(String resourceType) {
        this.resourceType = resourceType
    }

    String getComment() {
        return comment
    }

    void setComment(String comment) {
        this.comment = comment
    }

    ActorDTO getAppointment() {
        return appointment
    }

    void setAppointment(ActorDTO appointment) {
        this.appointment = appointment
    }
}

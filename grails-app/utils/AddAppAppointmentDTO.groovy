class AddAppAppointmentDTO {

    private String resourceType;
    private String status;
    private List<ValueDTO> identifier;
    private List<SimpleDTO> specialty;
    private List<SimpleDTO> reason;
    private String description;
    private String Comment;
    private String start;
    private String end;
    private String created;
    private List<ParticipantDTO> participant;
    private List<PeriodDTO> requestedPeriod;


    String getResourceType() {
        return resourceType
    }

    void setResourceType(String resourceType) {
        this.resourceType = resourceType
    }

    String getStatus() {
        return status
    }

    void setStatus(String status) {
        this.status = status
    }

    List<ValueDTO> getIdentifier() {
        return identifier
    }

    void setIdentifier(List<ValueDTO> identifier) {
        this.identifier = identifier
    }

    List<SimpleDTO> getSpecialty() {
        return specialty
    }

    void setSpecialty(List<SimpleDTO> specialty) {
        this.specialty = specialty
    }

    List<SimpleDTO> getReason() {
        return reason
    }

    void setReason(List<SimpleDTO> reason) {
        this.reason = reason
    }

    String getDescription() {
        return description
    }

    void setDescription(String description) {
        this.description = description
    }

    String getComment() {
        return Comment
    }

    void setComment(String comment) {
        Comment = comment
    }

    String getStart() {
        return start
    }

    void setStart(String start) {
        this.start = start
    }

    String getEnd() {
        return end
    }

    void setEnd(String end) {
        this.end = end
    }

    String getCreated() {
        return created
    }

    void setCreated(String created) {
        this.created = created
    }

    List<ParticipantDTO> getParticipant() {
        return participant
    }

    void setParticipant(List<ParticipantDTO> participant) {
        this.participant = participant
    }

    List<PeriodDTO> getRequestedPeriod() {
        return requestedPeriod
    }

    void setRequestedPeriod(List<PeriodDTO> requestedPeriod) {
        this.requestedPeriod = requestedPeriod
    }


    @Override
    public String toString() {
        return "AddAppAppointmentDTO{" +
                "resourceType='" + resourceType + '\'' +
                ", status='" + status + '\'' +
                ", specialty=" + specialty +
                ", reason=" + reason +
                ", description='" + description + '\'' +
                ", Comment='" + Comment + '\'' +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", created='" + created + '\'' +
                ", participant=" + participant +
                ", requestedPeriod=" + requestedPeriod +
                '}';
    }
}

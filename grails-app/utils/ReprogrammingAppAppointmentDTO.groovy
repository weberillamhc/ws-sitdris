class ReprogrammingAppAppointmentDTO {
    private String resourceType;
    private String status;
    private String id;
    private String start;
    private String end;
    private String created;
    private List<ParticipantDTO> participant;
    private List<PeriodDTO> requestedPeriod;

    String getResourceType() {
        return resourceType
    }

    void setResourceType(String resourceType) {
        this.resourceType = resourceType
    }

    String getStatus() {
        return status
    }

    void setStatus(String status) {
        this.status = status
    }

    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }

    String getStart() {
        return start
    }

    void setStart(String start) {
        this.start = start
    }

    String getEnd() {
        return end
    }

    void setEnd(String end) {
        this.end = end
    }

    String getCreated() {
        return created
    }

    void setCreated(String created) {
        this.created = created
    }

    List<ParticipantDTO> getParticipant() {
        return participant
    }

    void setParticipant(List<ParticipantDTO> participant) {
        this.participant = participant
    }

    List<PeriodDTO> getRequestedPeriod() {
        return requestedPeriod
    }

    void setRequestedPeriod(List<PeriodDTO> requestedPeriod) {
        this.requestedPeriod = requestedPeriod
    }
}

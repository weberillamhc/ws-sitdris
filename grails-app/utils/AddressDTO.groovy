class AddressDTO {
    private String resourseType
    private List<String> line
    private String city
    private String district
    private String state
    private String postalCode

    String getResourseType() {
        return resourseType
    }

    void setResourseType(String resourseType) {
        this.resourseType = resourseType
    }

    List<String> getLine() {
        return line
    }

    void setLine(List<String> line) {
        this.line = line
    }

    String getCity() {
        return city
    }

    void setCity(String city) {
        this.city = city
    }

    String getDistrict() {
        return district
    }

    void setDistrict(String district) {
        this.district = district
    }

    String getState() {
        return state
    }

    void setState(String state) {
        this.state = state
    }

    String getPostalCode() {
        return postalCode
    }

    void setPostalCode(String postalCode) {
        this.postalCode = postalCode
    }
}

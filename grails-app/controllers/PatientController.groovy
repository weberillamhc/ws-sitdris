import grails.converters.JSON
import grails.converters.XML
//import org.joda.time.DateTime
import java.text.DateFormat
import java.text.SimpleDateFormat


class PatientController {

    AppointmentService appointmentService
    ResponseService responseService
    ValidService validService

    def save() {

        def objectJSON = request.JSON
        AddPatientDTO addPatient = new AddPatientDTO(objectJSON)
        PatientResponseDTO responseDTO = new PatientResponseDTO();
        String formatDate = "yyyy-MM-dd";
        Patient patient = null
        Patient.withTransaction { status ->
            try {

                List<IdentifierDTO> identifiers = addPatient.getIdentifier()
                List<NameDTO> name = addPatient.getName()
                List<TelecomDTO> telecom = addPatient.getTelecom()
                List<AddressDTO> address = addPatient.getAddress()
                List<MaritalStatusDTO> maritalStatus = addPatient.getMaritialStatus()
                List<GeneralPractitionerDTO> generalPractitioner = addPatient.getGeneralPractitioner()
                List<ExtensionDTO> extension = addPatient.getExtension()

                 //Inicio validacion
                if (validService.emptyList(identifiers) || validService.isNull(identifiers[0])) {
                    responseDTO = responseService.buildResponsePatientError("El campo identificador del paciente es requerido.", "MSGE0002")
                    throw new Exception("MSGE0002")
                }

                if (validService.emptyList(name) || validService.isNull(name[0])) {
                    responseDTO = responseService.buildResponsePatientError("Nombre(s) del paciente requerido(s).", "MSGE0014")
                    throw new Exception("MSGE0014")
                }
                if (validService.emptyList(telecom) || validService.isNull(telecom[0])) {
                    responseDTO = responseService.buildResponsePatientError("El campo telecom del paciente es requerido.", "MSGE0002")
                    throw new Exception("MSGE0002")
                }
                if (validService.emptyList(address) || validService.isNull(address[0])) {
                    responseDTO = responseService.buildResponsePatientError("El campo dirección del paciente es requerido.", "MSGE0002")
                    throw new Exception("MSGE0002")
                }
                if (validService.emptyList(maritalStatus) || validService.isNull(maritalStatus[0])) {
                    responseDTO = responseService.buildResponsePatientError("El campo estado civil del paciente es requerido.", "MSGE0002")
                    throw new Exception("MSGE0002")
                }
                if (validService.emptyList(generalPractitioner) || validService.isNull(generalPractitioner[0])) {
                    responseDTO = responseService.buildResponsePatientError("El campo organización es requerido.", "MSGE0002")
                    throw new Exception("MSGE0002")
                }
                if (validService.emptyList(extension) || validService.isNull(extension[0])) {
                    responseDTO = responseService.buildResponsePatientError("El campo extensión es requerido.", "MSGE0002")
                    throw new Exception("MSGE0002")
                }

                if (validService.isNull(addPatient.getBirthDate())) {
                    responseDTO = responseService.buildResponsePatientError("El campo fecha de nacimiento es requerido.", "MSGE0002")
                    throw new Exception("MSGE0002")
                }

                if (validService.isNull(addPatient.getGender())) {
                    responseDTO = responseService.buildResponsePatientError("El campo sexo es requerido.", "MSGE0002")
                    throw new Exception("MSGE0002")
                }


                //Fin validacion


                //ParticipantDTO patient = participants[0]

                patient = new Patient();
                patient.patientType=1
                patient.idee = addPatient.getIdentifier()[0].value
                patient.name = addPatient.getName()[0].given[0]
                patient.firstLastname = addPatient.getName()[0].family
                patient.secondLastname = addPatient.getName()[0].suffix
                patient.gender = addPatient.getGender()
                patient.birthDate = validService.getDateByPattern(addPatient.getBirthDate(), formatDate)
                patient.curp = addPatient.getIdentifier()[1].value
                patient.origin = null
                patient.budgetKey=null
                patient.contractedServiceCode = addPatient.getGeneralPractitioner()[0].identifier[0].value
                patient.supplierRfc =null
                patient.observation=null
                patient.street=addPatient.getAddress()[0].line[0]
                patient.colony=addPatient.getAddress()[0].district
                patient.city=addPatient.getAddress()[0].city
                patient.state=addPatient.getAddress()[0].state
                patient.postalCode=addPatient.getAddress()[0].postalCode
                patient.maritalStatus=addPatient.getMaritialStatus()[0].coding
                patient.mobilePhone = addPatient.getTelecom()[1].value
                patient.homePhone = addPatient.getTelecom()[0].value
                patient.imssNumber = addPatient.getIdentifier()[2].value
                patient.medicalAggregated = addPatient.getIdentifier()[3].value
                patient.statusMedicalService =addPatient.getExtension()[2].valueInteger


                patient.validate()
                patient.save(flush: true, failOnError: true)
                responseDTO = responseService.buildResponsePatientSuccess("${patient.getId()}")

            } catch (e) {
                status.setRollbackOnly()
                println e.getMessage()
                e.printStackTrace()
            }
        }
        render(responseDTO as JSON)
    }
}

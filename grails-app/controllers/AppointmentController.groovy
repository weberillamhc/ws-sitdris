import grails.converters.JSON


class AppointmentController {


    AppointmentService appointmentService
    ResponseService responseService
    ValidService validService
    String formatDate = "yyyy-MM-dd'T'HH:mm:ss";
    String formatDateWithoutHour = "yyyy-MM-dd";

    def save() {

        def objectJSON = request.JSON
        AddAppAppointmentDTO addAppAppointmentDTO = new AddAppAppointmentDTO(objectJSON)
        AppointmentResponseDTO responseDTO = new AppointmentResponseDTO();
        MedicalAppointment appointment = null


        MedicalAppointment.withTransaction { status ->
            try {

                List<ParticipantDTO> participants = addAppAppointmentDTO.getParticipant()

                //Inicio validacion
                if (validService.emptyList(participants) || validService.isNull(participants[0])) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo identificador del paciente es requerido.", "MSGE0002")
                    throw new Exception("MSGE0002")
                }

                if (validService.isNull(addAppAppointmentDTO.getStatus())) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo estatus es requerido.", "MSGE0003")
                    throw new Exception("MSGE0003")
                }
                if (validService.emptyList(addAppAppointmentDTO.getSpecialty()) || validService.isNull(addAppAppointmentDTO.getSpecialty()[0])) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo clave especialidad es requerido.", "MSGE0027")
                    throw new Exception("MSGE0027")
                }

                if (validService.isNull(addAppAppointmentDTO.getDescription())) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo clave turno es requerido..", "MSGE0028")
                    throw new Exception("MSGE0028")
                }

                if (!validService.isSize(participants, 6) || validService.isNull(participants.get(5)
                        || validService.isNull(participants.get(5).getActor()) || validService.isNull(participants.get(5).getActor().getId()))) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo clave consultorio origen es requerido.", "MSGE0028")
                    throw new Exception("MSGE0028")
                }

                //Fin validacion


                ParticipantDTO patient = participants[0]

                appointment = new MedicalAppointment();
                appointment.status = addAppAppointmentDTO.getStatus()
                appointment.specialty = addAppAppointmentDTO.getSpecialty()[0].text
                appointment.shippingReason = addAppAppointmentDTO.getReason()[0].text
                appointment.turnAttention = addAppAppointmentDTO.description
                appointment.comment = addAppAppointmentDTO.comment

                appointment.startAppointment = validService.getDateByPattern(addAppAppointmentDTO.getStart(), formatDate)
                appointment.endAppointment = validService.getDateByPattern(addAppAppointmentDTO.getEnd(), formatDate)
                appointment.registrationDate = validService.getDateByPattern(addAppAppointmentDTO.getCreated(), formatDateWithoutHour)


                if (patient) {
                    appointment.serviceOccasion = patient.getType()[0].text
                    Patient patientEntity = Patient.findByIdOrIdee(patient.actor.id, patient.actor.id)
                    if (!patientEntity) {
                        return appointment
                    }
                    appointment.patient = patientEntity
                }

                ParticipantDTO practitioner1 = participants.get(1)
                if (practitioner1) {
                    //Clave de personal a atender
                    appointment.personalAttendingKey = practitioner1.getActor().getId()
                }

                ParticipantDTO practitioner2 = participants.get(2)
                if (practitioner2) {
                    //Clave de m�dico
                    appointment.doctorKey = practitioner2.getActor().getId()
                }

                ParticipantDTO practitioner3 = participants.get(3)
                if (practitioner3) {
                    ///Clave presupuestal de env�o
                    appointment.destinationBudgetKey = practitioner3.getActor().getId()
                }

                ParticipantDTO practitioner4 = participants.get(4)
                if (practitioner4) {
                    ///Clave  presupuestal que registra la cita
                    appointment.originBudgetKeyRegistration = practitioner4.getActor().getId()
                }

                ParticipantDTO practitioner5 = participants.get(5)
                if (practitioner5) {
                    ///Consultorio de la cita origen
                    appointment.originDoctorOffice = practitioner5.getActor().getId()
                }

                ParticipantDTO practitioner6 = participants.get(6)
                if (practitioner6) {
                    ///Consultorio de la cita destino
                    appointment.destinationDoctorOffice = practitioner6.getActor().getId()
                }

                PeriodDTO requestedPeriod = addAppAppointmentDTO.getRequestedPeriod()[0]
                if (requestedPeriod) {
                    //Fecha y hora inicio de la nueva cita
                    appointment.startNewAppointment = validService.getDateByPattern(requestedPeriod.getStart(), formatDate)
                    //Fecha y hora fin de la nueva cita
                    appointment.endNewAppointment = validService.getDateByPattern(requestedPeriod.getEnd(), formatDate)
                }

                appointment.validate()
                appointment.save(flush: true, failOnError: true)
                responseDTO = responseService.buildResponseAppointmentSuccess("${appointment.getId()}")

            } catch (e) {
                status.setRollbackOnly()
                println e.getMessage()
                e.printStackTrace()
            }
        }
        render(responseDTO as JSON)

    }

    def update(){
        def objectJSON = request.JSON
        AddAppAppointmentDTO updateAppAppointmentDTO = new AddAppAppointmentDTO(objectJSON)
        AppointmentResponseDTO responseDTO = new AppointmentResponseDTO();
        MedicalAppointment appointment = null
        MedicalAppointment.withTransaction { status ->
            try{
                List<ParticipantDTO> participants = updateAppAppointmentDTO.getParticipant()
                //Inicio validacion
                if (validService.emptyList(updateAppAppointmentDTO.getIdentifier()) || validService.isNull(updateAppAppointmentDTO.getIdentifier()[0].value)) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo identificador de la cita es requerido.", "MSGE0001")
                    throw new Exception("MSGE0001")
                }

                if (validService.emptyList(participants) || validService.isNull(participants[0])) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo identificador del paciente es requerido.", "MSGE0002")
                    throw new Exception("MSGE0002")
                }

                if (validService.isNull(updateAppAppointmentDTO.getStatus())) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo estatus es requerido.", "MSGE0003")
                    throw new Exception("MSGE0003")
                }
                if (validService.emptyList(participants) || validService.isNull(participants[3])) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo clave presupuestal destino es requerido", "MSGE0026")
                    throw new Exception("MSGE0026")
                }

                if (validService.emptyList(updateAppAppointmentDTO.getSpecialty()) || validService.isNull(updateAppAppointmentDTO.getSpecialty()[0])) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo clave especialidad es requerido.", "MSGE0027")
                    throw new Exception("MSGE0027")
                }

                if (validService.isNull(updateAppAppointmentDTO.getDescription())) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo clave turno es requerido..", "MSGE0028")
                    throw new Exception("MSGE0028")
                }

                if (!validService.isSize(participants, 6) || validService.isNull(participants.get(5)
                        || validService.isNull(participants.get(5).getActor()) || validService.isNull(participants.get(5).getActor().getId()))) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo clave consultorio origen es requerido.", "MSGE0029")
                    throw new Exception("MSGE0029")
                }

                if (!validService.isSize(participants, 6) || validService.isNull(participants.get(2)
                        || validService.isNull(participants.get(2).getActor()) || validService.isNull(participants.get(2).getActor().getId()))) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo clave matr�cula m�dico es requerido.", "MSGE0030")
                    throw new Exception("MSGE0030")
                }

                if (validService.isNull(updateAppAppointmentDTO.getStart())) {
                    responseDTO = responseService.buildResponseAppointmentError("La fecha y hora de inicio de la cita es requerida.", "MSGE0031")
                    throw new Exception("MSGE0031")
                }

                if (validService.isNull(updateAppAppointmentDTO.getEnd())) {
                    responseDTO = responseService.buildResponseAppointmentError("La fecha y hora de fin de la cita es requerida.", "MSGE0032")
                    throw new Exception("MSGE0032")
                }

                if (validService.isNull(updateAppAppointmentDTO.getCreated())) {
                    responseDTO = responseService.buildResponseAppointmentError("La fecha de modificaci�n de la cita es requerida.", "MSGE0055")
                    throw new Exception("MSGE0055")
                }

                if (!validService.isSize(participants, 6) || validService.isNull(participants.get(4)
                        || validService.isNull(participants.get(4).getActor()) || validService.isNull(participants.get(4).getActor().getId()))) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo clave presupuestal que registra es requerido.", "MSGE0035")
                    throw new Exception("MSGE0035")
                }

                //Fin validacion

                String identifier = updateAppAppointmentDTO.getIdentifier()[0].value;
                ParticipantDTO patient = participants[0]
                appointment = MedicalAppointment.findByIdOrIdentifier(identifier, identifier);
                appointment.identifier = identifier
                appointment.status = updateAppAppointmentDTO.getStatus()
                appointment.specialty = updateAppAppointmentDTO.getSpecialty()[0].text
                appointment.shippingReason = updateAppAppointmentDTO.getReason()[0].text
                appointment.turnAttention = updateAppAppointmentDTO.getDescription()
                appointment.startAppointment = validService.getDateByPattern(updateAppAppointmentDTO.getStart(), formatDate)
                appointment.endAppointment = validService.getDateByPattern(updateAppAppointmentDTO.getEnd(), formatDate)
                appointment.modificationDate = validService.getDateByPattern(updateAppAppointmentDTO.getCreated(), formatDateWithoutHour)

                if (patient) {
                    appointment.serviceOccasion = patient.getType()[0].text
                    Patient patientEntity = Patient.findByIdOrIdee(patient.actor.id, patient.actor.id)
                    if (!patientEntity) {
                        return appointment
                    }
                    appointment.patient = patientEntity
                }

                ParticipantDTO practitioner1 = participants.get(1)
                if (practitioner1) {
                    //Clave de personal a atender
                    appointment.personalAttendingKey = practitioner1.getActor().getId()
                }

                ParticipantDTO practitioner2 = participants.get(2)
                if (practitioner2) {
                    //Clave de m�dico
                    appointment.doctorKey = practitioner2.getActor().getId()
                }

                ParticipantDTO practitioner3 = participants.get(3)
                if (practitioner3) {
                    ///Clave presupuestal de env�o
                    appointment.destinationBudgetKey = practitioner3.getActor().getId()
                }

                ParticipantDTO practitioner4 = participants.get(4)
                if (practitioner4) {
                    ///Clave  presupuestal que registra la cita
                    appointment.originBudgetKeyRegistration = practitioner4.getActor().getId()
                }

                ParticipantDTO practitioner5 = participants.get(5)
                if (practitioner5) {
                    ///Consultorio de la cita origen
                    appointment.originDoctorOffice = practitioner5.getActor().getId()
                }

                ParticipantDTO practitioner6 = participants.get(6)
                if (practitioner6) {
                    ///Consultorio de la cita destino
                    appointment.destinationDoctorOffice = practitioner6.getActor().getId()
                }

                appointment.validate()
                appointment.save(flush: true, failOnError: true)
                responseDTO = responseService.buildResponseAppointmentSuccess("${appointment.getId()}")

            }catch(e){
                status.setRollbackOnly()
                println e.getMessage()
                e.printStackTrace()
            }
        }
        render(responseDTO as JSON)
    }

    def delete () {
        def objectJSON = request.JSON
        AddAppAppointmentDTO deleteAppAppointmentDTO = new AddAppAppointmentDTO(objectJSON)
        AppointmentResponseDTO responseDTO = new AppointmentResponseDTO();
        MedicalAppointment appointment = null
        MedicalAppointment.withTransaction { status ->
            try{
                List<ParticipantDTO> participants = deleteAppAppointmentDTO.getParticipant()
                //Inicio validacion
                if (validService.emptyList(deleteAppAppointmentDTO.getIdentifier()) || validService.isNull(deleteAppAppointmentDTO.getIdentifier()[0].value)) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo identificador de la cita es requerido.", "MSGE0001")
                    throw new Exception("MSGE0001")
                }

                if (validService.emptyList(participants) || validService.isNull(participants[0])) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo identificador del paciente es requerido.", "MSGE0002")
                    throw new Exception("MSGE0002")
                }

                if (validService.isNull(deleteAppAppointmentDTO.getStatus())) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo estatus es requerido.", "MSGE0003")
                    throw new Exception("MSGE0003")
                }

                if (validService.emptyList(participants) || validService.isNull(participants[2])) {
                    responseDTO = responseService.buildResponseAppointmentError("Clave Presupuestal que cancela es requerida.", "MSGE0004")
                    throw new Exception("MSGE0004")
                }

                if (validService.emptyList(participants) || validService.isNull(participants[1])) {
                    responseDTO = responseService.buildResponseAppointmentError("Matr�cula del personal que cancela la cita es requerida.", "MSGE0005")
                    throw new Exception("MSGE0005")
                }

                if (validService.emptyList(deleteAppAppointmentDTO.getReason()) || validService.isNull(deleteAppAppointmentDTO.getReason()[0].text)) {
                    responseDTO = responseService.buildResponseAppointmentError("El motivo de la cancelaci�n es requerido.", "MSGE0006")
                    throw new Exception("MSGE0006")
                }

                if (validService.isNull(deleteAppAppointmentDTO.getCreated())) {
                    responseDTO = responseService.buildResponseAppointmentError("La fecha de env�o de cancelaci�n es requerida.", "MSGE0007")
                    throw new Exception("MSGE0007")
                }

                //Fin validacion

                String identifier = deleteAppAppointmentDTO.getIdentifier()[0].value;
                ParticipantDTO patient = participants[0]
                appointment = MedicalAppointment.findByIdOrIdentifier(identifier, identifier);
                appointment.status = deleteAppAppointmentDTO.getStatus()
                appointment.dateCancellation = validService.getDateByPattern(deleteAppAppointmentDTO.getCreated(), formatDateWithoutHour)
                appointment.cancellationReason = deleteAppAppointmentDTO.getReason()[0].text

                if (patient) {
                    Patient patientEntity = Patient.findByIdOrIdee(patient.actor.id, patient.actor.id)
                    if (!patientEntity) {
                        return appointment
                    }
                    appointment.patient = patientEntity
                }

                ParticipantDTO practitioner1 = participants.get(1)
                if (practitioner1) {
                    //Matr�cula del personal que cancela
                    appointment.employeeKeyCancellation = practitioner1.getActor().getId()
                }

                ParticipantDTO location1 = participants.get(2)
                if (location1) {
                    //Clave presupuestal que cancela
                    appointment.cancellationBudgetKey = location1.getActor().getId()
                }

                appointment.validate()
                appointment.save(flush: true, failOnError: true)
                responseDTO = responseService.buildResponseAppointmentSuccess("${appointment.getId()}")

            }catch(e){
                status.setRollbackOnly()
                println e.getMessage()
                e.printStackTrace()
            }
        }
        render(responseDTO as JSON)
    }

    def reprogramming () {
        def objectJSON = request.JSON
        ReprogrammingAppAppointmentDTO reprogrammingAppAppointmentDTO = new ReprogrammingAppAppointmentDTO(objectJSON)
        AppointmentResponseDTO responseDTO = new AppointmentResponseDTO();
        MedicalAppointment appointment = null
        MedicalAppointment.withTransaction { status ->
            try{
                List<ParticipantDTO> participants = reprogrammingAppAppointmentDTO.getParticipant()
                //Inicio validacion
                if (validService.isNull(reprogrammingAppAppointmentDTO.getId())) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo identificador de la cita es requerido.", "MSGE0001")
                    throw new Exception("MSGE0001")
                }

                if (validService.emptyList(participants) || validService.isNull(participants[0])) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo identificador del paciente es requerido.", "MSGE0002")
                    throw new Exception("MSGE0002")
                }

                if (validService.isNull(reprogrammingAppAppointmentDTO.getStatus())) {
                    responseDTO = responseService.buildResponseAppointmentError("El campo estatus es requerido.", "MSGE0003")
                    throw new Exception("MSGE0003")
                }
                if (!validService.isSize(participants, 4) || validService.isNull(participants.get(3)
                    || validService.isNull(participants.get(3).getActor()) || validService.isNull(participants.get(3).getActor().getId()))) {
                    responseDTO = responseService.buildResponseAppointmentError("Clave Presupuestal que reprograma es requerida.", "MSGE0052")
                    throw new Exception("MSGE0052")
                }

                if (validService.isNull(reprogrammingAppAppointmentDTO.getStart())) {
                    responseDTO = responseService.buildResponseAppointmentError("La fecha y hora de inicio de la cita es requerida.", "MSGE0031")
                    throw new Exception("MSGE0031")
                }

                if (validService.isNull(reprogrammingAppAppointmentDTO.getEnd())) {
                    responseDTO = responseService.buildResponseAppointmentError("La fecha y hora de fin de la cita es requerida.", "MSGE0032")
                    throw new Exception("MSGE0032")
                }

                if (validService.isNull(reprogrammingAppAppointmentDTO.getCreated())) {
                    responseDTO = responseService.buildResponseAppointmentError("La fecha de env�o en la que se reprograma la cita es requerida.", "MSGE0056")
                    throw new Exception("MSGE0056")
                }

                //Fin validacion
                String id = reprogrammingAppAppointmentDTO.getId()[0]
                ParticipantDTO patient = participants[0]
                appointment = MedicalAppointment.findByIdOrIdentifier(id, id);
                appointment.status = reprogrammingAppAppointmentDTO.getStatus()
                appointment.startAppointment = validService.getDateByPattern(reprogrammingAppAppointmentDTO.getStart(), formatDate)
                appointment.endAppointment = validService.getDateByPattern(reprogrammingAppAppointmentDTO.getEnd(), formatDate)
                appointment.dateReprogramming = validService.getDateByPattern(reprogrammingAppAppointmentDTO.getCreated(), formatDateWithoutHour)

                if (patient) {
                    Patient patientEntity = Patient.findByIdOrIdee(patient.actor.id, patient.actor.id)
                    if (!patientEntity) {
                        return appointment
                    }
                    appointment.patient = patientEntity
                }

                ParticipantDTO practitioner1 = participants.get(1)
                if (practitioner1) {
                    //Matr�cula del personal que reprograma la cita
                    appointment.reprogrammingEmployeeKey = practitioner1.getActor().getId()
                }

                ParticipantDTO location1 = participants.get(2)
                if (location1) {
                    //Clave presupuestal de la Unidad m�dica de atenci�n que reprograma la cita
                    appointment.reprogrammingBudgetKey = location1.getActor().getId()
                }

                ParticipantDTO location2 = participants.get(3)
                if (location2) {
                    //Clave presupuestal de la Unidad m�dica de atenci�n que atender� la cita
                    appointment.reprogrammingDestinationBudgetKey = location2.getActor().getId()
                }

                PeriodDTO requestedPeriod = reprogrammingAppAppointmentDTO.getRequestedPeriod()[0]
                if (requestedPeriod) {
                    //Fecha y hora inicio de la nueva cita
                    appointment.startNewAppointment = validService.getDateByPattern(requestedPeriod.getStart(), formatDate)
                    //Fecha y hora fin de la nueva cita
                    appointment.endNewAppointment = validService.getDateByPattern(requestedPeriod.getEnd(), formatDate)
                }

                appointment.validate()
                appointment.save(flush: true, failOnError: true)
                responseDTO = responseService.buildResponseAppointmentSuccess("${appointment.getId()}")

            }catch(e){
                status.setRollbackOnly()
                println e.getMessage()
                e.printStackTrace()
            }
        }
        render(responseDTO as JSON)
    }


}
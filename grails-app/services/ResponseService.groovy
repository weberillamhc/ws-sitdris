
import java.text.SimpleDateFormat

class ResponseService {



    AppointmentResponseDTO buildResponseAppointmentSuccess(String id){
        AppointmentResponseDTO response = new AppointmentResponseDTO();
        response.setResourceType("Bundle")
        response.setId("bundle-response")
        response.setType("transaction-response")


        ResourceDTO resource = new ResourceDTO()
        resource.setResourceType("AppointmentResponse")
        resource.setAppointment(new ActorDTO(reference: "Appointment/1", id: id))
        resource.setComment("La solicitud se ha realizado con \u00e9xito.")

        ResponseDTO responseDTO = new ResponseDTO(status: "201 OK")
        EntryResponseDTO entryResponse = new EntryResponseDTO(resource: resource, response: responseDTO)

        List<EntryResponseDTO> entry = new ArrayList<EntryResponseDTO>()
        entry.add(entryResponse)
        response.setEntry(entry)

        return response
    }

    AppointmentResponseDTO buildResponseAppointmentError(String message, String code){
        AppointmentResponseDTO response = new AppointmentResponseDTO();
        response.setResourceType("Bundle")
        response.setId("bundle-response")
        response.setType("transaction-response")


        ResourceDTO resource = new ResourceDTO()
        resource.setResourceType("AppointmentResponse")
        resource.setAppointment(new ActorDTO(reference: "Appointment/1", id: "ND"))
        resource.setComment("No se tiene conexi\u00f3n con la Aplicaci\u00f3n Receptora.")

        OutComeDTO outcome = new OutComeDTO(resourceType: "OperationOutcome", issue: new ArrayList<IssueDTO>());
        outcome.getIssue().add(new IssueDTO(severity: "error", details: new SimpleDTO(text: message, code: code)))

        ResponseDTO responseDTO = new ResponseDTO(status: "401", outcome: outcome)
        EntryResponseDTO entryResponse = new EntryResponseDTO(resource: resource, response: responseDTO)

        List<EntryResponseDTO> entry = new ArrayList<EntryResponseDTO>()
        entry.add(entryResponse)
        response.setEntry(entry)
        return response
    }

    PatientResponseDTO buildResponsePatientSuccess(String id){
        PatientResponseDTO response = new PatientResponseDTO();
        response.setResourceType("Bundle")
        response.setId("bundle-response")
        response.setType("transaction-response")


        ResourceDTO resource = new ResourceDTO()
        resource.setResourceType("PatientResponse")
        resource.setAppointment(new ActorDTO(reference: "Appointment/1", id: id))
        resource.setComment("La solicitud se ha realizado con \u00e9xito.")

        ResponseDTO responseDTO = new ResponseDTO(status: "201 OK")
        EntryResponseDTO entryResponse = new EntryResponseDTO(resource: resource, response: responseDTO)

        List<EntryResponseDTO> entry = new ArrayList<EntryResponseDTO>()
        entry.add(entryResponse)
        response.setEntry(entry)

        return response
    }

    PatientResponseDTO buildResponsePatientError(String message, String code){
        PatientResponseDTO response = new PatientResponseDTO();
        response.setResourceType("Bundle")
        response.setId("bundle-response")
        response.setType("transaction-response")


        ResourceDTO resource = new ResourceDTO()
        resource.setResourceType("PatientResponse")
        resource.setAppointment(new ActorDTO(reference: "Appointment/1", id: "ND"))
        resource.setComment("No se tiene conexi\u00f3n con la Aplicaci\u00f3n Receptora.")

        OutComeDTO outcome = new OutComeDTO(resourceType: "OperationOutcome", issue: new ArrayList<IssueDTO>());
        outcome.getIssue().add(new IssueDTO(severity: "error", details: new SimpleDTO(text: message, code: code)))

        ResponseDTO responseDTO = new ResponseDTO(status: "401", outcome: outcome)
        EntryResponseDTO entryResponse = new EntryResponseDTO(resource: resource, response: responseDTO)

        List<EntryResponseDTO> entry = new ArrayList<EntryResponseDTO>()
        entry.add(entryResponse)
        response.setEntry(entry)
        return response
    }

}

import java.sql.Timestamp
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.regex.Pattern

class ValidService {

    boolean isNull(Object object) {
        return object == null;
    }


    boolean nonNull(Object object) {
        return !isNull(object);
    }

    boolean nonEmpty(String value) {
        return nonNull(value) && !value.isEmpty() && !"null".equals(value);
    }

    boolean emptyList(List<?> list) {
        return !nonEmptyList(list)
    }
    boolean nonEmptyList(List<?> list) {
        return nonNull(list) && !list.isEmpty();
    }

    boolean isSize(List<?> list, int size) {
        return nonEmptyList(list)  && (list.size() >=size)
    }
    boolean isAlphabetic(String cadena) {
        Pattern pat = Pattern.compile("[A-Za-z]");
        return pat.matcher(cadena).find();
    }

    boolean isAlphaNumeric(String cadena) {
        Pattern pat = Pattern.compile("[A-Za-z0-9]");
        return pat.matcher(cadena).find();
    }



    Date getDateByPattern(String dateStr, String pattern) {
        try {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
            return dateFormat.parse(dateStr);
        } catch (ParseException e) {
            return null;
        }
    }

    boolean isNotValidLength(String value, long maxlength) {
        return nonEmpty(value) && value.length() > maxlength;
    }


    Timestamp now() {
        return  new Timestamp(new Date().getTime());
    }

    Timestamp dateToTimestamp(Date date) {
        return  new Timestamp(date.getTime());
    }

    String formatDate(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return simpleDateFormat.format(date);
    }
}

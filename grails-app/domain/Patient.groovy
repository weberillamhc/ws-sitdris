class Patient {

	String idee
	String name
	String firstLastname
	String secondLastname
	String gender
	Date birthDate
	String origin
	String budgetKey
	String curp
	Date registrationDate
	String contractedServiceCode
	String supplierRfc
	String agreementType
	String observation
	String street
	String colony
	String city
	String state
	String postalCode
	String maritalStatus
	String mobilePhone
	String homePhone
	Integer patientType
	String imssNumber
	String patientNumber
	String medicalAggregated
	Date deathDate
	String deatchReason
	Integer statusMedicalService
	Integer statusRightDisability
	String employerRegistration
	String contractNumber
	Date effectiveDate

	static hasMany = [medicalAppointments: MedicalAppointment,
	                  studies: Study]

	static mapping = {
		id column: "id_patient", generator: "identity"
		version false
	}

	static constraints = {
		idee maxSize: 350
		name maxSize: 350
		firstLastname maxSize: 350
		secondLastname nullable: true, maxSize: 350
		gender maxSize: 300
		origin nullable: true, maxSize: 300
		budgetKey nullable: true, maxSize: 300
		curp maxSize: 30
		registrationDate nullable: true
		contractedServiceCode nullable: true, maxSize: 20
		supplierRfc nullable: true, maxSize: 50
		agreementType nullable: true, maxSize: 30
		observation nullable: true, maxSize: 600
		street nullable: true
		colony nullable: true
		city nullable: true
		state nullable: true
		postalCode nullable: true, maxSize: 10
		maritalStatus nullable: true, maxSize: 10
		mobilePhone nullable: true, maxSize: 15
		homePhone nullable: true, maxSize: 15
		patientType nulleable: true
		imssNumber nullable: true, maxSize: 100
		patientNumber nullable: true, maxSize: 100
		medicalAggregated nullable: true
		deathDate nullable: true
		deatchReason nullable: true
		statusMedicalService nullable: true
		statusRightDisability nullable: true
		employerRegistration nullable: true, maxSize: 300
		contractNumber nullable: true, maxSize: 300
		effectiveDate nullable: true
	}
}

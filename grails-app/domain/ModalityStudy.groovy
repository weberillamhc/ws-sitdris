
class ModalityStudy {

	String modality
	String interpretation
	String interpreterName
	String interpreterLastname
	String interpreterSecondLastname
	Date studyDate
	Short status
	Short studyType
	Short quantityImages
	String endpointStatus
	String endpointPayloadType
	String endpointAddress
	String uid
	Study study

	static belongsTo = [Study]

	static mapping = {
		id column: "id_modality_study", generator: "assigned"
		version false
	}

	static constraints = {
		modality maxSize: 300
		interpretation nullable: true
		interpreterName nullable: true
		interpreterLastname nullable: true
		interpreterSecondLastname nullable: true
		studyDate nullable: true
		status nullable: true
		studyType nullable: true
		quantityImages nullable: true
		endpointStatus nullable: true, maxSize: 50
		endpointPayloadType nullable: true, maxSize: 50
		endpointAddress nullable: true, maxSize: 200
		uid nullable: true, maxSize: 300
	}
}

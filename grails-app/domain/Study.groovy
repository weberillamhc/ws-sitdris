class Study {

	Date registrationDate
	Date startStudy
	Date endStudy
	Short status
	MedicalAppointment medicalAppointment
	Patient patient

	static hasMany = [modalityStudies: ModalityStudy]
	static belongsTo = [MedicalAppointment, Patient]

	static mapping = {
		id column: "id_study", generator: "assigned"
		version false
	}

	static constraints = {
		startStudy nullable: true
		endStudy nullable: true
		status nullable: true
	}
}

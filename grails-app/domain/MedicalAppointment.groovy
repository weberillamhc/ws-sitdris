
class MedicalAppointment {

	String destinationDoctorOffice
	String turnAttention
	String identifier
	String specialty
	String shippingReason
	String turn
	String comment
	Date startAppointment
	Date endAppointment
	Date registrationDate
	String status
	String presumptiveDiagnosis
	String originDoctorOffice
	String doctorKey
	String personalAttendingKey
	Date startNewAppointment
	Date endNewAppointment
	String originBudgetKeyRegistration
	String destinationBudgetKey
	String serviceOccasion
	Date modificationDate
	String cancellationReason
	String employeeKeyCancellation
	String cancellationBudgetKey
	Date dateCancellation
	String reprogrammingBudgetKey
	String reprogrammingDestinationBudgetKey
	String reprogrammingEmployeeKey
	Date dateReprogramming
	Patient patient

	static hasMany = [studies: Study]
	static belongsTo = [Patient]

	static mapping = {
		id column: "id_medical_appointment", generator: "identity"
		version false
	}

	static constraints = {
		destinationDoctorOffice maxSize: 200
		turnAttention maxSize: 200
		identifier nullable: true, maxSize: 16
		specialty nullable: true, maxSize: 300
		shippingReason nullable: true, maxSize: 500
		turn nullable: true, maxSize: 22
		comment nullable: true, maxSize: 200
		startAppointment nullable: true
		endAppointment nullable: true
		registrationDate nullable: true
		status nullable: true, maxSize: 300
		presumptiveDiagnosis nullable: true
		originDoctorOffice nullable: true, maxSize: 200
		doctorKey nullable: true, maxSize: 200
		personalAttendingKey nullable: true, maxSize: 200
		startNewAppointment nullable: true
		endNewAppointment nullable: true
		originBudgetKeyRegistration nullable: true, maxSize: 200
		destinationBudgetKey nullable: true, maxSize: 200
		serviceOccasion nullable: true, maxSize: 200
		modificationDate nullable: true
		cancellationReason nullable: true
		employeeKeyCancellation nullable: true, maxSize: 300
		cancellationBudgetKey nullable: true, maxSize: 200
		dateCancellation nullable: true
		reprogrammingBudgetKey nullable: true, maxSize: 200
		reprogrammingDestinationBudgetKey nullable: true, maxSize: 200
		reprogrammingEmployeeKey nullable: true, maxSize: 200
		dateReprogramming nullable: true
	}
}
